lu = require('luaunit')
local sign = require("sign")
local sha2 = require("sha2")

function TestPutAWSExample()
    local key = "AKIAIOSFODNN7EXAMPLE"
    local secret = "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"
    local time = os.time({year = 2013, month = 5, day = 24, hour = 0})
    local date = os.date('%a, %d %b %Y %H:%M:%S GMT', time)
    local path = "/test%24file.text"
    local region = "us-east-1"
    local contentSha256 = sha2.hash256("Welcome to Amazon S3.")
    local headers = {["x-amz-storage-class"] = "REDUCED_REDUNDANCY", ["x-amz-date"] = "20130524T000000Z", ["x-amz-content-sha256"] = contentSha256, ["host"] = "examplebucket.s3.amazonaws.com", ["date"] = date }
    local signature = sign.sign(key, secret, time,path, headers, region)
    lu.assertEquals( type(signature), 'string' )
    lu.assertEquals( signature, "AWS4-HMAC-SHA256 Credential=AKIAIOSFODNN7EXAMPLE/20130524/us-east-1/s3/aws4_request,SignedHeaders=date;host;x-amz-content-sha256;x-amz-date;x-amz-storage-class,Signature=98ad721746da40c64f1a55b78f14c238d841ea1380cd77a1b5971af0ece108bd" )
end

function TestPut()
    local key = "AKIAIOSFODNN7EXAMPLE"
    local secret = "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"
    local time = os.time({year = 2019, month = 6, day = 28, hour = 23, min = 50, sec = 58})
    local date = os.date('%a, %d %b %Y %H:%M:%S GMT', time)
    local path = "/20190628/d98e63986091dc0eb0fa24cff9fffaf17b90c464c2b1f05685bc91e7585267cb0d09c645e69ef2b7"
    local region = "eu-west-1"
    local contentSha256 = sha2.hash256("fluff2")
    local headers = {["x-amz-acl"] = "public-read", ["x-amz-date"] = "20190628T235058Z", ["x-amz-content-sha256"] = contentSha256, ["host"] = "upload.unbound.se.s3-eu-west-1.amazonaws.com", ["date"] = date }
    local signature = sign.sign(key, secret, time,path, headers, region)
    lu.assertEquals( type(signature), 'string' )
    lu.assertEquals( signature, "AWS4-HMAC-SHA256 Credential=AKIAIOSFODNN7EXAMPLE/20190628/eu-west-1/s3/aws4_request,SignedHeaders=date;host;x-amz-acl;x-amz-content-sha256;x-amz-date,Signature=6dd94b1ff7fd29251dbec629b813ea974323aa6509b6a3db94744aef386914af" )
end

-- class TestSign

os.exit(lu.LuaUnit:run())
