local sign = {}

local hmac = require("hmac")
local sha2 = require("sha2")

function sign.sign(key, secret, time, path, headers, region)
        local day = os.date("%Y%m%d", time)
        local timestamp = os.date("%Y%m%dT%H%M%SZ", time)
        local dateKey = hmac.hmac("AWS4" .. secret, day, 'buffer')
        local dateRegionKey = hmac.hmac(dateKey, region, 'buffer')
        local dateRegionSvcKey = hmac.hmac(dateRegionKey, 's3', 'buffer')
        local signingKey = hmac.hmac(dateRegionSvcKey, 'aws4_request', 'buffer')

        local keys = {}
        for k, v in pairs(headers) do
                table.insert(keys, k)
        end
        table.sort(keys)

        local signedHeaders = ""
        local request = "PUT\n" .. path .. "\n"
        for _, k in ipairs(keys) do
                request = request .. "\n" .. k .. ":" .. headers[k]
                signedHeaders = signedHeaders .. ";" .. k
        end
        signedHeaders = string.sub(signedHeaders, 2)

        request = request .. "\n\n" .. signedHeaders .. "\n" .. (headers["x-amz-content-sha256"])
        local stringToSign = "AWS4-HMAC-SHA256\n" .. timestamp  .. "\n" .. day .. "/" .. region .. "/s3/aws4_request\n" .. sha2.hash256(request)
        local signature = hmac.hmac(signingKey, stringToSign, 'hex')

        local result = "AWS4-HMAC-SHA256 Credential=" .. key .. "/" .. day .. "/" .. region .. "/s3/aws4_request,SignedHeaders=" .. signedHeaders
        result = result .. ",Signature=" .. signature
        return result
end

return sign
