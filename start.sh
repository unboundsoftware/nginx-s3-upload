#!/bin/bash

set +u

set -euo pipefail
: ${S3_BUCKET_NAME:?"S3_BUCKET_NAME must be set"}
: ${AWS_REGION:?"AWS_REGION must be set"}
: ${RETURN_URL:?"RETURN_URL must be set"}

exec nginx -g 'daemon off;'
