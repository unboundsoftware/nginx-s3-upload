apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: nginx-s3-upload
  name: nginx-s3-upload
  annotations:
    kubernetes.io/change-cause: "${TIMESTAMP} Deployed commit id: ${COMMIT}"
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx-s3-upload
  strategy:
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: nginx-s3-upload
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: "app"
                  operator: In
                  values:
                  - nginx-s3-upload
              topologyKey: kubernetes.io/hostname
      containers:
      - name: nginx-s3-upload
        resources:
          requests:
            memory: 200Mi
          limits:
            memory: 300Mi
        readinessProbe:
          httpGet:
            path: /healthcheck
            port: 80
          initialDelaySeconds: 5
          periodSeconds: 6
          timeoutSeconds: 5
        imagePullPolicy: Always
        image: registry.gitlab.com/unboundsoftware/nginx-s3-upload:${COMMIT}
        env:
          - name: S3_BUCKET_NAME
            value: upload.unbound.se
          - name: AWS_REGION
            value: eu-west-1
          - name: RETURN_URL
            value: uploads.unbound.se
        ports:
        - containerPort: 80

---

apiVersion: v1
kind: Service
metadata:
  name: nginx-s3-upload
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: nginx-s3-upload
  type: NodePort

---

apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-s3-upload
  annotations:
    kubernetes.io/ingress.class: "alb"
    alb.ingress.kubernetes.io/group.name: "unbound"
    alb.ingress.kubernetes.io/scheme: internet-facing
    alb.ingress.kubernetes.io/target-type: instance
    alb.ingress.kubernetes.io/listen-ports: '[{"HTTP": 80},{"HTTPS": 443}]'
    alb.ingress.kubernetes.io/ssl-redirect: "443"
spec:
  rules:
  - host: 'upload.unbound.se'
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: nginx-s3-upload
            port:
              number: 80
