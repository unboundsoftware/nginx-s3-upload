local fetcher = {}

function fetcher.fetch()
  local key = os.getenv("AWS_ACCESS_KEY_ID")
  local secret = os.getenv("AWS_SECRET_ACCESS_KEY")
  if (key and secret) then
    ngx.log(ngx.STDERR, "Key: " .. key .. ", Secret: " .. secret)
    return key, secret, nil
  end

  local http = require "resty.http"
  local JSON = require "JSON"
  local httpc = http.new()
  local res, err = httpc:request_uri("http://169.254.169.254/latest/meta-data/iam/security-credentials/", { method = "GET" })
  if not res then
    ngx.log(ngx.STDERR, "failed to get IAM role: ", err)
    return
  end

  local role = res.body
  res, err = httpc:request_uri("http://169.254.169.254/latest/meta-data/iam/security-credentials/" .. role, { method = "GET" })
  if not res then
    ngx.log(ngx.STDERR, "failed to get role info: ", err)
    return
  end
  local json = res.body
  local table = JSON:decode(json)
  key = table["AccessKeyId"]
  secret = table["SecretAccessKey"]
  local token = table["Token"]
  return key, secret, token
end

return fetcher
