FROM ubuntu

# Install prerequisites for Nginx compile
RUN apt-get update && \
    apt-get install -y wget tar gcc libpcre3-dev zlib1g-dev make libssl-dev liblua5.1-0 lua-bit32 libluajit-5.1-dev curl jq

# Download Nginx
WORKDIR /tmp
RUN wget http://nginx.org/download/nginx-1.16.0.tar.gz -O nginx.tar.gz && \
    mkdir nginx && \
    tar xf nginx.tar.gz -C nginx --strip-components=1

# Download Nginx modules
RUN wget https://github.com/simpl/ngx_devel_kit/archive/v0.3.0.tar.gz -O ngx_devel_kit.tar.gz && \
    mkdir ngx_devel_kit && \
    tar xf ngx_devel_kit.tar.gz -C ngx_devel_kit --strip-components=1 && \
    wget https://github.com/openresty/set-misc-nginx-module/archive/v0.32.tar.gz -O set-misc-nginx-module.tar.gz && \
    mkdir set-misc-nginx-module && \
    tar xf set-misc-nginx-module.tar.gz -C set-misc-nginx-module --strip-components=1 && \
    wget https://github.com/openresty/lua-resty-core/archive/v0.1.17.tar.gz -O lua-resty-core.tar.gz && \
    mkdir lua-resty-core && \
    tar xf lua-resty-core.tar.gz -C lua-resty-core --strip-components=1 && \
    wget https://github.com/ledgetech/lua-resty-http/archive/v0.14.tar.gz -O lua-resty-http.tar.gz && \
    mkdir lua-resty-http && \
    tar xf lua-resty-http.tar.gz -C lua-resty-http --strip-components=1 && \
    wget https://github.com/openresty/lua-nginx-module/archive/v0.10.15.tar.gz -O lua-nginx-module.tar.gz && \
    mkdir lua-nginx-module && \
    tar xf lua-nginx-module.tar.gz -C lua-nginx-module --strip-components=1 && \
    wget https://github.com/openresty/luajit2/archive/v2.1-20190530.tar.gz -O luajit2.tar.gz && \
    mkdir luajit2 && \
    tar xf luajit2.tar.gz -C luajit2 --strip-components=1 && \
    cd /tmp/luajit2 && \
    make && \
    make install


# Build Nginx
WORKDIR nginx
RUN LUAJIT_LIB=/usr/local/lib LUAJIT_INC=/usr/local/include/luajit-2.1 ./configure --sbin-path=/usr/local/sbin \
                --conf-path=/etc/nginx/nginx.conf \
                --pid-path=/var/run/nginx.pid \
                --error-log-path=/var/log/nginx/error.log \
                --http-log-path=/var/log/nginx/access.log \
                --with-http_ssl_module \
                --with-ld-opt="-Wl,-rpath,/usr/local/lib" \
                --add-module=/tmp/ngx_devel_kit \
                --add-module=/tmp/set-misc-nginx-module \
                --add-module=/tmp/lua-nginx-module && \
    make && \
    make install


# Expose ports
EXPOSE 80

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
        && ln -sf /dev/stderr /var/log/nginx/error.log

# Apply Nginx config
ADD nginx.conf /etc/nginx/nginx.conf
ADD fetcher.lua hmac.lua JSON.lua sha2.lua sign.lua /tmp/lua/
ADD start.sh /start.sh

# Set default command
ENTRYPOINT ["/start.sh"]
